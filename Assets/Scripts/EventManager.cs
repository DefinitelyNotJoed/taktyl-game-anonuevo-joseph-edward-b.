﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvent {}

public class EventManager
{
    public delegate void EventDelegate<Event>(Event e) where Event : GameEvent;

    private Dictionary<Type, Delegate> eDict = new Dictionary<Type, Delegate>();
    private static EventManager _instance;
    public static EventManager instance
    {
        get
        {
            if (_instance == null)
                _instance = new EventManager();
            return _instance;
        }
    }
    
    public static void AddListener<Event>(EventDelegate<Event> listener) where Event : GameEvent
    {
        Delegate del;

        // Check if event type exist
        if (instance.eDict.TryGetValue(typeof(Event), out del))
            instance.eDict[typeof(Event)] = Delegate.Combine(del, listener); // Combine listeners
        else
            instance.eDict[typeof(Event)] = listener; // Assign Event type to a listener
    }

    public static void RemoveListener<Event>(EventDelegate<Event> listener) where Event : GameEvent
    {
        Delegate del;
        
        // Check if event type exist
        if (instance.eDict.TryGetValue(typeof(Event), out del))
        {
            Delegate d = Delegate.Remove(del, listener);

            // Check if event delegate still has a listener
            if (d == null)
                instance.eDict.Remove(typeof(Event)); // Remove Event type if listener is null
            else
                instance.eDict[typeof(Event)] = d; // Do not remove event type if listener is not null
        }
    }

    public static void Raise<Event>(Event e) where Event : GameEvent
    {
        if (e == null)
            throw new ArgumentNullException("[" + typeof(Event).Name + "] event does not exist");
        
        Delegate del;

        // Call back
        if (instance.eDict.TryGetValue(typeof(Event), out del))
        {
            EventDelegate<Event> call = del as EventDelegate<Event>;
            if (call != null)
                call(e);
        }
    }
}