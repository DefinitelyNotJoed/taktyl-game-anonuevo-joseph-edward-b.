﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthHandler : MonoBehaviour
{
    protected Health health;

    private void Awake()
    {
        health = GetComponent<Health>();
        Init();
    }

    public virtual void TakeDamage(float damage)
    {
        health.ReduceHealth(damage);
    }

    protected virtual void Init()
    {
    }
}
