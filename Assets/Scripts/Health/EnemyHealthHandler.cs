﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthHandler : HealthHandler, IDestroyable
{
    private EnemyType type;

    private bool isVisible;

    protected override void Init()
    {
        base.Init();

        type = GetComponent<Enemy>().enemyType;
    }

    private void Start()
    {
        // To know if health is at 0
        health.deathDel += OnDeath;
    }

    public override void TakeDamage(float damage)
    {
        // Damage calcs here

        base.TakeDamage(damage);
    }

    public void OnDeath()
    {
        // Unsubscribe to event if health falls down to 0
        health.deathDel -= OnDeath;

        KillEvent e = new KillEvent();
        e.enemyType = type;
        EventManager.Raise(e);
        
        Destroy(this.gameObject);
    }

    private void OnBecameInvisible()
    {
        // Unsubscribe to event if object is out of screen
        health.deathDel -= OnDeath;
        Destroy(this.gameObject);
    }
}
