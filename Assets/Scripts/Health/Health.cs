﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public delegate void DeathDelegate();
    public DeathDelegate deathDel;

    [SerializeField] private float currentHealth;
    [SerializeField] private float maxHealth = 50;

    public float GetHealth
    {
        get { return currentHealth; }
        private set { }
    }

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void ReduceHealth(float damage)
    {
        // Reduce Health
        currentHealth -= damage;

        // Call a local event if health drops to 0
        if (currentHealth <= 0)
        {
            if (deathDel != null)
                deathDel();
        }
    }
}
