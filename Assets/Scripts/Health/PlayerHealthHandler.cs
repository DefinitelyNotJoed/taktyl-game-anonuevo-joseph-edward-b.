﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthHandler : HealthHandler, IDestroyable
{
    [SerializeField] private float invulnerabilityTimer = 2;
    [SerializeField] private float enemyColissionDamage = 10;

    private Collider col;
    private MeshRenderer meshRenderer;
    private bool isInvulnerable;

    protected override void Init()
    {
        base.Init();

        col = GetComponent<Collider>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        // To know if health is at 0
        health.deathDel += OnDeath;
        HealthUI.SetHealth(health.GetHealth);
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        HealthUI.SetHealth(health.GetHealth);

        StartCoroutine(InvulnerableTimer());
    }

    private IEnumerator InvulnerableTimer()
    {
        // Enable Invulnerablity
        isInvulnerable = true;
        col.enabled = false;
        StartCoroutine(Blink());

        yield return new WaitForSeconds(invulnerabilityTimer);

        // Disable Invulnerability
        isInvulnerable = false;
        StopCoroutine(Blink());
        col.enabled = true;
        meshRenderer.enabled = true;
    }

    private IEnumerator Blink()
    {
        while(isInvulnerable == true)
        {
            meshRenderer.enabled = !meshRenderer.enabled;
            yield return new WaitForSeconds(0.25f);
        }
    }

    public void OnDeath()
    {
        // Unsubscribe to event if health falls down to 0
        health.deathDel -= OnDeath;
        Destroy(this.gameObject);

        GameOverUI.OnGameOver();
    }
}
