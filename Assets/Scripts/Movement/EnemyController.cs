﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float speed = 40f;
    [SerializeField] private float distanceToStop = 0.1f;
    [SerializeField] private Transform target;

    private bool isMoving;

    public void Stop()
    {
        isMoving = false;

        target = null;
    }

    private void Update()
    {
        if (target == null)
            return;

        // Get direction
        Vector3 dir = (target.position - transform.position).normalized;
        dir.y = 0;

        // Move towards target
        transform.Translate(-dir * speed * Time.deltaTime);

        // Check if near the target
        if (Vector3.Distance(target.position, transform.position) < distanceToStop)
            Stop();
    }
}
