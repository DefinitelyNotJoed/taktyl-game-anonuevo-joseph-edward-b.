﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 40f;

    private Vector3 clampedPos;

    private void Update()
    {
        // Get Axis input
        float moveH = Input.GetAxisRaw("Horizontal");
        float moveV = Input.GetAxisRaw("Vertical");

        // Set movement vector
        Vector3 movement = new Vector3(moveH, 0f, moveV) * speed * Time.deltaTime;

        // Clamp Player movement
        clampedPos = Camera.main.WorldToViewportPoint(transform.position);
        clampedPos.x = Mathf.Clamp(clampedPos.x, 0.05f, 0.95f);
        clampedPos.y = Mathf.Clamp(clampedPos.y, 0.07f, 0.93f);
        transform.position = Camera.main.ViewportToWorldPoint(clampedPos);

        transform.position += movement;
    }
}
