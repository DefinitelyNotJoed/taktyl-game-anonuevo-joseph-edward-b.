﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    [SerializeField] private float speed = 0.25f;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        // Scale background to window size

        transform.localScale = new Vector3(Camera.main.orthographicSize * 2.5f * Screen.width / Screen.height, 
           0.1f, Camera.main.orthographicSize * 2.5f);
    }
    
    private void Update()
    {
        // Scroll material
        Vector2 offset = new Vector2(0, -Time.time * speed);

        meshRenderer.material.mainTextureOffset = offset;
    }
}
