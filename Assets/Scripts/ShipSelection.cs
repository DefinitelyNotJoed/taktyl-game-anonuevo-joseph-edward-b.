﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipSelection : MonoBehaviour
{
    [SerializeField] private GameObject fighterShip;
    [SerializeField] private GameObject transportShip;

    private GameObject prefab;

    public void OnClickFighter()
    {
        prefab = fighterShip;
        OnShipSelect();
    }

    public void OnClickTransport()
    {
        prefab = transportShip;
        OnShipSelect();
    }

    private void OnShipSelect()
    {
        SceneManager.LoadScene("MainScene");
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level == 1)
        {
            GameObject.Instantiate(prefab, prefab.transform.position, prefab.transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
