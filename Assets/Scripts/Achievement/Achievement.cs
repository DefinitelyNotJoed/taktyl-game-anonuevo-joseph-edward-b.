﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementEvent : GameEvent
{
    public string eventName;
}

public abstract class AchievementConditionEvent : GameEvent
{
}

public class Achievement : MonoBehaviour
{
    [Header("General")]
    public string achievementName;
    public string description;
    public int requiredValue;
    public int currentValue;
    
    public bool isCompleted;

    protected bool isActive;

    public virtual void AddToActiveList()
    {
        // Adds the achievement to active list
        isActive = true;

        EventManager.AddListener<AchievementEvent>(OnEventComplete);
    }

    public virtual void RemoveFromActiveList()
    {
        // Removes the achievement to active list
        isActive = false;

        EventManager.RemoveListener<AchievementEvent>(OnEventComplete);
    }

    public void Reset()
    {
        // Reset achievement
        currentValue = 0;
        isCompleted = false;
    }

    private void OnEventComplete(AchievementEvent e)
    {
        // Shows UI and remove achievement from active list
        if (e.eventName != achievementName ||
            isCompleted)
            return;
        
        AchievementUI.OnAchievementComplete(achievementName);

        isCompleted = true;
        RemoveFromActiveList();
    }

    protected virtual void EventCondition()
    {
        // Event conditions for local
    }

    protected virtual void EventCondition(AchievementConditionEvent e)
    {
        // Event conditions for event types
    }
}
