﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour
{
    public Achievement[] achievement;

    private void Start()
    {
        // Places all achievements in list to active
        for (int i = 0; i < achievement.Length; i++)
            achievement[i].AddToActiveList();
    }
}
