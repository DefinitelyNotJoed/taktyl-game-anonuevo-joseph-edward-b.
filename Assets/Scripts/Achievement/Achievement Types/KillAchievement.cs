﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEvent : AchievementConditionEvent
{
    public EnemyType enemyType;
}

public class KillAchievement : Achievement
{
    [Header("Kill Conditions")]
    public EnemyType enemyType;

    public override void AddToActiveList()
    {
        base.AddToActiveList();

        EventManager.AddListener<KillEvent>(EventCondition);
    }

    public override void RemoveFromActiveList()
    {
        base.RemoveFromActiveList();

        EventManager.RemoveListener<KillEvent>(EventCondition);
    }

    protected override void EventCondition(AchievementConditionEvent e)
    {
        // Checks if the unit killed is this achievement's enemy type
        KillEvent killEvent = e as KillEvent; // Implicit cast to Kill Event
        if (killEvent.enemyType != this.enemyType)
            return;

        currentValue++;

        if(currentValue >= requiredValue)
        {
            AchievementEvent ae = new AchievementEvent();
            ae.eventName = achievementName;
            EventManager.Raise(ae);
        }
    }
}
