﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputAchievement : Achievement
{
    private void Update()
    {
        if (isActive == false)
            return;
        
        EventCondition();
    }

    protected override void EventCondition()
    {
        if (Input.anyKey)
        {
            AchievementEvent ae = new AchievementEvent();
            ae.eventName = this.achievementName;
            EventManager.Raise(ae);
        }
    }
}
