﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IDestroyable
{
    protected float damage;
    protected float moveSpeed;

    protected Vector3 targetPosition;
    protected Vector3 defaultPosition = new Vector3(0, 200, 0);

    protected MeshRenderer meshRenderer;
    protected Collider col;

    public float Damage
    {
        get { return damage; }
        private set { }
    }

    public bool isActive
    {
        get;
        private set;
    }

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        col = GetComponent<Collider>();
    }

    private void Start()
    {
        OnDeath();
    }

    public void OnDeath()
    {
        // Place the object outside camera bounds
        col.enabled = false;
        meshRenderer.enabled = false;
        transform.position = defaultPosition;

        isActive = false;
    }

    private void Update()
    {
        if (!meshRenderer.enabled)
            return;

        Move();
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckCollision(other);
    }

    protected void Move()
    {
        transform.position += targetPosition * moveSpeed * Time.deltaTime;
    }

    protected virtual void CheckCollision(Collider other)
    {
        if (transform.position == defaultPosition)
            return;

        // Check if collider has health
        HealthHandler hp = other.GetComponent<HealthHandler>();

        if (hp == null)
            return;

        // Store bullet
        OnDeath();
        
        // Deal damage
        hp.TakeDamage(Damage);
    }

    public void BulletInitialize(Vector3 target, Vector3 origin, float speed = 40, float Damage = 1)
    {
        // Initialize values
        meshRenderer.enabled = true;
        col.enabled = true;

        transform.position = origin;
        targetPosition = target;
        moveSpeed = speed;
        isActive = true;

        damage = 1;
    }

    private void OnBecameInvisible()
    {
        OnDeath();
    }
}