﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    [SerializeField] private GameObject BulletPrefab;
    [SerializeField] private int BulletCache = 50;

    private GameObject parent;
    private GameObject bulletCacheGO;
    private GameObject[] bullet;
    private int lastBullet = -1;
    private Bullet[] bulletCompCache;

    private bool isInitialized;

    private void Awake()
    {
        InitializeCache();
    }

    public void InitializeCache()
    {
        if(isInitialized)
        {
            Debug.Log("Bullet cache is already initialized");
            return;
        }

        parent = transform.gameObject;
        bulletCacheGO = new GameObject { name = "Bullet Cache (" + gameObject.name + ")" };

        // Initialize Array
        bullet = new GameObject[BulletCache];
        bulletCompCache = new Bullet[BulletCache];

        // Preload and pool bullets
        for (int i = 0; i < bullet.Length; i++)
        {
            bullet[i] = Instantiate(BulletPrefab) as GameObject;
            bullet[i].name = "" + i;
            bullet[i].transform.SetParent(bulletCacheGO.transform);
            bulletCompCache[i] = bullet[i].GetComponent<Bullet>();
        }
            
        isInitialized = true;
    }

    public Bullet GetNextBullet()
    {
        // Returns the next bullet not in use
        lastBullet += 1;
        if (lastBullet > BulletCache - 1)
            lastBullet = 0;

        return bulletCompCache[lastBullet];
    }

    public void DestroyCache()
    {
        Destroy(bulletCacheGO);
        isInitialized = false;
    }
}
