﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Mooks,
    HarderMooks
}

public class Enemy : MonoBehaviour
{
    public string enemyName;
    public int level = 1;
    public EnemyType enemyType;
}
