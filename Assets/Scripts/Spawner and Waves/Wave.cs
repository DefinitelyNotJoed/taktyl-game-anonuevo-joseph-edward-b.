﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private Transform spawnLocation;
    [SerializeField] private float spawnCount = 1;
    [SerializeField] private float timeBetweenSpawn = 1.5f;

    public void StartWave(EnemySpawner enemySpawner)
    {
        StartCoroutine(SpawnTimer(enemySpawner));
    }

    private IEnumerator SpawnTimer(EnemySpawner enemySpawner)
    {
        for (int i = 0; i < spawnCount; i++)
        {
            enemySpawner.SpawnWithLocation(prefab, spawnLocation.position, prefab.transform.rotation);
            yield return new WaitForSeconds(timeBetweenSpawn);
        }

        EventManager.Raise(new WaveEndEvent());
    }
}
