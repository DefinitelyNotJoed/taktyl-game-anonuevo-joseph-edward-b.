﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveEndEvent : GameEvent
{
}

[RequireComponent(typeof(EnemySpawner))]
public class WaveHandler : MonoBehaviour
{
    [SerializeField] private Wave[] waves;
    [SerializeField] private float timeBetweenWaves = 3f;

    private EnemySpawner enemySpawner;
    private int index = -1;

    private void Awake()
    {
        enemySpawner = GetComponent<EnemySpawner>();
    }

    private void Start()
    {
        EventManager.AddListener<WaveEndEvent>(OnWaveEnd);
        NextWave();
    }

    private void NextWave()
    {
        index++;
        if (index >= waves.Length)
            index = 0;

        waves[index].StartWave(enemySpawner);
    }

    private void OnWaveEnd(WaveEndEvent e)
    {
        StartCoroutine(WaveTimer());
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener<WaveEndEvent>(OnWaveEnd);
    }

    private IEnumerator WaveTimer()
    {
        yield return new WaitForSeconds(timeBetweenWaves);
        NextWave();
    }
}
