﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public void SpawnWithLocation(GameObject prefab, Vector3 location, Quaternion rotation)
    {
        GameObject.Instantiate(prefab, location, rotation);
    }
}