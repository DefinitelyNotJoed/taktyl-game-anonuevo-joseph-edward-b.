﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : ShootHandler
{
    public bool IsAbleToShoot { get; protected set; }

    protected override void ShootInput()
    {
        if (!IsAbleToShoot)
            return;

        if (shotTimer < shotInterval)
            return;

        shooting.Shoot(forward);
        shotTimer = 0;
    }

    private void OnBecameInvisible()
    {
        IsAbleToShoot = false;
    }

    private void OnBecameVisible()
    {
        IsAbleToShoot = true;
    }
}
