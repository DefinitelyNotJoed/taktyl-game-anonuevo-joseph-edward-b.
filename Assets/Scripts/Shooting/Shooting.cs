﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private Transform[] muzzle;
    [SerializeField] private float speed = 40;
    [SerializeField] private float damage = 1;
    [SerializeField] private float damageMult = 1;

    private BulletManager bulletManager;

    private void Awake()
    {
        bulletManager = GetComponent<BulletManager>();

        if (muzzle.Length == 0)
        {
            muzzle = new Transform[1];
            muzzle[0] = transform;
        }
    }

    public void Shoot()
    {
        for (int i = 0; i < muzzle.Length; i++)
        {
            // Get the next bullet and initialize
            Bullet bullet = bulletManager.GetNextBullet();
            
            bullet.BulletInitialize(muzzle[i].forward, muzzle[i].position, speed, damage * damageMult);
        }
    }

    public void Shoot(Vector3 target)
    {
        for (int i = 0; i < muzzle.Length; i++)
        {
            // Get the next bullet and initialize
            Bullet bullet = bulletManager.GetNextBullet();

            bullet.BulletInitialize(target, muzzle[i].position, speed, damage * damageMult);
        }
    }
}
