﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpinShoot : EnemyShoot
{
    private int angle = 0;

    protected override void ShootInput()
    {
        angle += 1;
        forward.x = (1 * Mathf.Cos(angle * Mathf.PI / 180));
        forward.z = (1 * Mathf.Sin(angle * Mathf.PI / 180));

        base.ShootInput();
    }
}
