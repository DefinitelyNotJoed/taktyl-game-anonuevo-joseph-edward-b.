﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootHandler : MonoBehaviour
{
    [SerializeField] protected Vector3 forward;
    [SerializeField] protected float shotInterval = 0.15f;

    protected Shooting shooting;
    protected float shotTimer;

    private void Awake()
    {
        shooting = GetComponent<Shooting>();
    }

    private void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        shotTimer = shotInterval;
    }

    private void Update()
    {
        shotTimer += Time.deltaTime;
        ShootInput();
    }

    protected virtual void ShootInput()
    {
    }
}
