﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : ShootHandler
{
    protected override void ShootInput()
    {
        if (shotTimer < shotInterval)
            return;

        if (Input.GetButton("Fire1"))
        {
            shooting.Shoot();
            shotTimer = 0;
        }
    }
}