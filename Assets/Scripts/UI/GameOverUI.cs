﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour
{
    public Canvas gameOverPanel;

    private static GameOverUI _instance;
    public static GameOverUI instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(GameOverUI)) as GameOverUI;
                if (_instance == null)
                    throw new System.ArgumentNullException("Attach modal UI to a game object");
            }
            return _instance;
        }
    }

    public static void OnGameOver()
    {
        instance.gameOverPanel.enabled = true;
        instance.StartCoroutine(instance.Timer());
    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(3);

        instance.gameOverPanel.enabled = false;
        SceneManager.LoadScene("TitleScene");
    }
}
