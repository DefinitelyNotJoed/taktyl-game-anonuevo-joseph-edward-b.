﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    public Text hp;

    private static HealthUI _instance;
    public static HealthUI instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(HealthUI)) as HealthUI;
                if (_instance == null)
                    throw new System.ArgumentNullException("Attach modal UI to a game object");
            }
            return _instance;
        }
    }

    public static void SetHealth(float i)
    {
        instance.hp.text = "HP: " + i.ToString();
    }
}
