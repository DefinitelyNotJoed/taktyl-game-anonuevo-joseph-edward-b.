﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUI : MonoBehaviour
{
    public Canvas achievementPanel;
    public Text achievementText;

    private static AchievementUI _instance;
    public static AchievementUI instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(AchievementUI)) as AchievementUI;
                if (_instance == null)
                    throw new System.ArgumentNullException("Attach modal UI to a game object");
            }
            return _instance;
        }
    }

    public static void OnAchievementComplete(string text)
    {
        instance.achievementText.text = "Achievement Completed!\n" + text;
        instance.StartCoroutine(instance.ShowPanel());
    }

    private IEnumerator ShowPanel()
    {
        // Show panel for 3.5 seconds

        instance.achievementPanel.enabled = true;

        yield return new WaitForSeconds(3.5f);

        instance.achievementPanel.enabled = false;
    }
}
